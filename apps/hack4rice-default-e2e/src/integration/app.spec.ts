import { getGreeting } from '../support/app.po';

describe('hack4rice-default', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to hack4rice-default!');
  });
});
