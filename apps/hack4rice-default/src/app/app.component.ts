import { AfterViewInit, Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';
import { format } from 'date-fns';

import { environment } from '../environments/environment';

import { forkJoin, defer, Subject } from 'rxjs';
import { share, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'h4r-d-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  map: L.Map;
  collapsed = false;
  inputDate: any;
  selectedMunicipality = 'Muñoz';
  rainfallForecast = null;
  munozGeoJsonLayer = null;
  sanJoseGeoJsonLayer = null;
  santoDomingoGeoJsonLayer = null;
  submitButtonDisabled = false;
  httpRequestPipeline$ = new Subject();
  httpRequestPipelineObs$ = this.httpRequestPipeline$.asObservable().pipe(
    tap(() => {
      this.rainfallForecast = null;
      this.submitButtonDisabled = true;
    }),

    switchMap(() => {
      return forkJoin(
        defer(() => this._http.post('/api/forecast-rainfall/san-jose', { inputDate: format(this.inputDate, 'yyyy-MM-dd') })),
        this._http.get('/assets/nueva-ecija-san-jose-city.geojson'),
        this._http.get(`http://api.openweathermap.org/data/2.5/forecast?q=San%20Jose,ph&APPID=${environment.openWeatherMapApiKey}`).pipe(
          tap((data) => console.log('DEBUG:: San Jose', { data }))
        ),
        defer(() => this._http.post('/api/forecast-rainfall/munoz', { inputDate: format(this.inputDate, 'yyyy-MM-dd') })),
        this._http.get('/assets/nueva-ecija-munoz-city.geojson'),
        this._http.get(`http://api.openweathermap.org/data/2.5/forecast?q=Munoz,ph&APPID=${environment.openWeatherMapApiKey}`).pipe(
          tap((data) => console.log('DEBUG:: Muñoz', { data }))
        ),
        defer(() => this._http.post('/api/forecast-rainfall/santo-domingo', { inputDate: format(this.inputDate, 'yyyy-MM-dd') })),
        this._http.get('/assets/nueva-ecija-santo-domingo.geojson'),
        this._http.get(`http://api.openweathermap.org/data/2.5/forecast?q=Santo%20Domingo,ph&APPID=${environment.openWeatherMapApiKey}`).pipe(
          tap((data) => console.log('DEBUG:: Santo Domingo', { data }))
        ),
      );
    }),

    tap((data) => {
      this.submitButtonDisabled = false;

      console.log('DEBUG:: ', { data });
    }),

    share()
  );

  constructor(private _http: HttpClient) { }

  ngOnInit() {
    this.httpRequestPipelineObs$.subscribe(([sanJoseForecastRes, sanJoseGeoJsonRes, sanJoseOpenWeatherRes, munozForecastRes, munozGeoJsonRes, munozOpenWeatherRes, santoDomingoForecastRes, santoDomingoGeoJsonRes, santoDomingoOpenWeatherRes]: [any, any, any, any, any, any, any, any, any]) => {
      if (this.sanJoseGeoJsonLayer !== null) {
        this.sanJoseGeoJsonLayer.removeFrom(this.map);
        this.sanJoseGeoJsonLayer = null;
      }

      if (this.munozGeoJsonLayer !== null) {
        this.munozGeoJsonLayer.removeFrom(this.map);
        this.munozGeoJsonLayer = null;
      }

      if (this.santoDomingoGeoJsonLayer !== null) {
        this.santoDomingoGeoJsonLayer.removeFrom(this.map);
        this.santoDomingoGeoJsonLayer = null;
      }

      this.munozGeoJsonLayer = L.geoJSON(munozGeoJsonRes, this.generateGeoJSONConfig(munozForecastRes.rainfall_forecast))
        .bindPopup(
          `
            <span>Muñoz City: ${munozForecastRes.rainfall_forecast}mm</span>
            <br />
            <span>Historical Data: <a href="/assets/nueva-ecija-munoz-precip-2018-07-01-2019-07-31.csv" download>Download</a></span>
          `
        )
        .addTo(this.map)
        ;

      this.sanJoseGeoJsonLayer = L.geoJSON(sanJoseGeoJsonRes, this.generateGeoJSONConfig(sanJoseForecastRes.rainfall_forecast))
        .bindPopup(
          `
            <span>San Jose City: ${sanJoseForecastRes.rainfall_forecast}mm</span>
            <br />
            <span>Historical Data: <a href="/assets/nueva-ecija-san-jose-precip-2018-07-01-2019-07-31.csv" download>Download</a></span>
          `
        )
        .addTo(this.map)
        ;

      this.santoDomingoGeoJsonLayer = L.geoJSON(santoDomingoGeoJsonRes, this.generateGeoJSONConfig(santoDomingoForecastRes.rainfall_forecast))
        .bindPopup(
          `
            <span>Santo Domingo: ${santoDomingoForecastRes.rainfall_forecast}mm</span>
            <br />
            <span>Historical Data: <a href="/assets/nueva-ecija-santo-domingo-2018-07-01-2019-07-31.csv" download>Download</a></span>
          `
        )
        .addTo(this.map)
        ;

      this.rainfallForecast = sanJoseForecastRes.rainfall_forecast;
    });
  }

  ngAfterViewInit() {
    this.map = L.map('map').setView([15.602572388077315, 120.95412969589235], 11);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }

  toggleSideNav() {
    this.collapsed = !this.collapsed;
  }

  onSubmit() {
    this.httpRequestPipeline$.next();
    // const requests$ = forkJoin(
    //   this._http.get('/api/forecast-rainfall'),
    //   this._http.get('/assets/nueva-ecija-san-jose-city.geojson'),
    //   this._http.get('/api/forecast-rainfall'),
    //   this._http.get('/assets/nueva-ecija-munoz-city.geojson'),
    //   this._http.get('/api/forecast-rainfall'),
    //   this._http.get('/assets/nueva-ecija-santo-domingo.geojson')
    // );

    // this.rainfallForecast = null;
    // requests$.subscribe(([sanJoseForecastRes, sanJoseGeoJsonRes, munozForecastRes, munozGeoJsonRes, santoDomingoForecastRes, santoDomingoGeoJsonRes]: [any, any, any, any, any, any]) => {
    //   if (this.sanJoseGeoJsonLayer !== null) {
    //     this.sanJoseGeoJsonLayer.removeFrom(this.map);
    //     this.sanJoseGeoJsonLayer = null;
    //   }

    //   if (this.munozGeoJsonLayer !== null) {
    //     this.munozGeoJsonLayer.removeFrom(this.map);
    //     this.munozGeoJsonLayer = null;
    //   }

    //   if (this.santoDomingoGeoJsonLayer !== null) {
    //     this.santoDomingoGeoJsonLayer.removeFrom(this.map);
    //     this.santoDomingoGeoJsonLayer = null;
    //   }

    //   this.munozGeoJsonLayer = L.geoJSON(munozGeoJsonRes, this.generateGeoJSONConfig(munozForecastRes.rainfall_forecast))
    //     .bindPopup(`Muñoz City: ${munozForecastRes.rainfall_forecast}mm`)
    //     .addTo(this.map)
    //     ;

    //   this.sanJoseGeoJsonLayer = L.geoJSON(sanJoseGeoJsonRes, this.generateGeoJSONConfig(sanJoseForecastRes.rainfall_forecast))
    //     .bindPopup(`San Jose City: ${sanJoseForecastRes.rainfall_forecast}mm`)
    //     .addTo(this.map)
    //     ;

    //   this.santoDomingoGeoJsonLayer = L.geoJSON(santoDomingoGeoJsonRes, this.generateGeoJSONConfig(santoDomingoForecastRes.rainfall_forecast))
    //     .bindPopup(`Santo Domingo: ${santoDomingoForecastRes.rainfall_forecast}mm`)
    //     .addTo(this.map)
    //     ;

    //   this.rainfallForecast = sanJoseForecastRes.rainfall_forecast;
    // });
  }

  generateGeoJSONConfig(rainMMValue) {
    return {
      style: function (feature) {
        let color = '#000000';
        let opac = 0.7;

        if (rainMMValue < 50) {
          color = '#0070FF';
        } else if (rainMMValue > 50 && rainMMValue <= 100) {
          color = '#004DA8';
        } else if (rainMMValue > 100 && rainMMValue <= 150) {
          color = '#002673';
        } else if (rainMMValue > 150 && rainMMValue <= 200) {
          color = '#000000';
        }

        return {
          color,
          "fillOpacity": opac,
        };
      }
    };
  }

}
