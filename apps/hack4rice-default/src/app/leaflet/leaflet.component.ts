import { Component, OnInit } from '@angular/core';
// import plugin
import * as L from 'leaflet';
//import 'leaflet-routing-machine';

@Component({
  selector: 'h4r-d-leaflet',
  templateUrl: './leaflet.component.html',
  styleUrls: ['./leaflet.component.css']
})
export class LeafletComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const map = L.map('map').setView([15.5784, 121.1113], 11);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    /*L.Routing.control({
        waypoints: [
            L.latLng(57.74, 11.94),
            L.latLng(57.6792, 11.949)
        ]
    }).addTo(map);*/
  }

}
